﻿using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SimpleShop_API_NET.Models;

public class Customer
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    public string? Name { get; set; }
    public string? Address { get; set; }

    /**
     * default implicit here for json field name in Mongodb collection: [BsonElement("PhoneNumber")],
     * add to ignore auto format joins [JsonProp...] with property line :(
     */
    [BsonElement("PhoneNumber")]
    [JsonPropertyName("phone")]
    public string? PhoneNumber { get; set; }

    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}