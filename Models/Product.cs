﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SimpleShop_API_NET.Models;

public class Product
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    public string? Name { get; set; }
    public string? Description { get; set; }
    public string? Image { get; set; }
    public float Price { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}