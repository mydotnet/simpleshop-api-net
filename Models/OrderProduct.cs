﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SimpleShop_API_NET.Models;

public class OrderProduct
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    [BsonRepresentation(BsonType.ObjectId)]
    public string? OrderId { get; set; }

    [BsonRepresentation(BsonType.ObjectId)]
    public string? ProductId { get; set; }

    public string? ProductName { get; set; }
    public string? ProductDescription { get; set; }
    public string? ProductImage { get; set; }
    public float ProductPrice { get; set; }
    public int ProductQuantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}