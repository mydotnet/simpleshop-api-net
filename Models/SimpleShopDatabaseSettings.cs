﻿namespace SimpleShop_API_NET.Models;

public class SimpleShopDatabaseSettings
{
    public string ConnectionString { get; set; } = null!;
    public string DatabaseName { get; set; } = null!;
    public string CustomerCollectionName { get; set; } = null!;
    public string OrderCollectionName { get; set; } = null!;
    public string OrderProductCollectionName { get; set; } = null!;
    public string ProductCollectionName { get; set; } = null!;
}