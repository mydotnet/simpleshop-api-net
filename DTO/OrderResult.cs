﻿using System.Collections;
using System.Linq.Expressions;
using SimpleShop_API_NET.Models;

namespace SimpleShop_API_NET.DTO;

public class OrderResult
{
    public string? Id { get; set; }
    public string? CustomerId { get; set; }
    public string? ReceiverName { get; set; }
    public string? ReceiverAddress { get; set; }
    public string? ReceiverPhoneNumber { get; set; }
    public string? Note { get; set; }
    public float TotalAmount { get; set; }
    public int TotalQuantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public List<OrderProduct> OrderProducts { get; set; } = new List<OrderProduct>();
}