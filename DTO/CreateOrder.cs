﻿namespace SimpleShop_API_NET.DTO;

public class CreateOrder
{
    public string? CustomerId { get; set; }

    public string? ReceiverName { get; set; }
    public string? ReceiverAddress { get; set; }
    public string? ReceiverPhoneNumber { get; set; }
    public string? Note { get; set; }

    public List<CreateOrderProduct> CreateOrderProducts { get; set; }
}