﻿namespace SimpleShop_API_NET.DTO;

public class CreateOrderProduct
{
    public string? OrderId { get; set; }
    public string? ProductId { get; set; }
    public string? ProductName { get; set; }
    public string? ProductDescription { get; set; }
    public float ProductPrice { get; set; }
    public int ProductQuantity { get; set; }
}