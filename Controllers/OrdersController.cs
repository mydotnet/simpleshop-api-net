﻿using Microsoft.AspNetCore.Mvc;
using SimpleShop_API_NET.DTO;
using SimpleShop_API_NET.Models;
using SimpleShop_API_NET.Services;

namespace SimpleShop_API_NET.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrdersController : ControllerBase
{
    private readonly OrdersService _ordersService;

    public OrdersController(OrdersService ordersService)
    {
        _ordersService = ordersService;
    }

    [HttpGet]
    public async Task<List<OrderResult>> Get() =>
        await _ordersService.GetAsync();

    [HttpGet("{id:length(24)}")]
    public async Task<ActionResult<OrderResult>> Get(string id)
    {
        var order = await _ordersService.GetAsync(id);
        return (order is null) ? NotFound() : order;
    }

    [HttpGet("Orders/Simple")]
    public async Task<List<Order>> GetSimple() =>
        await _ordersService.GetSimpleAsync();

    [HttpGet("Orders/Simple/{id:length(24)}")]
    public async Task<ActionResult<Order>> GetSimple(string id)
    {
        var order = await _ordersService.GetSimpleAsync(id);
        return (order is null) ? NotFound() : order;
    }

    [HttpPost]
    public async Task<IActionResult> Post(CreateOrder createOrder)
    {
        try
        {
            var newOrder = await _ordersService.CreateAsync(createOrder);

            return CreatedAtAction(nameof(Get), new { id = newOrder.Id }, newOrder);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update(string id, Order updatedOrder)
    {
        var order = await _ordersService.GetSimpleAsync(id);
        if (order is null)
        {
            return NotFound();
        }

        updatedOrder.Id = order.Id;
        await _ordersService.UpdateAsync(id, updatedOrder);

        return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete(string id)
    {
        var order = await _ordersService.GetSimpleAsync(id);
        if (order is null)
        {
            return NotFound();
        }

        await _ordersService.RemoveAsync(id);

        return NoContent();
    }
}