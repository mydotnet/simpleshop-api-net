﻿using Microsoft.AspNetCore.Mvc;
using SimpleShop_API_NET.Models;
using SimpleShop_API_NET.Services;

namespace SimpleShop_API_NET.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ProductsController : ControllerBase
{
    private readonly ProductsService _productsService;

    public ProductsController(ProductsService productsService)
    {
        _productsService = productsService;
    }

    [HttpGet]
    public async Task<List<Product>> Get() =>
        await _productsService.getAsync();

    [HttpGet("{id:length(24)}")]
    public async Task<ActionResult<Product?>> Get(string id)
    {
        var product = await _productsService.getAsync(id);

        return (product is null) ? NotFound() : product;
    }

    [HttpPost]
    public async Task<IActionResult> Post(Product newProduct)
    {
        await _productsService.CreateAsync(newProduct);

        return CreatedAtAction(nameof(Get), new { id = newProduct.Id }, newProduct);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update(string id, Product updatedProduct)
    {
        var product = await _productsService.getAsync(id);
        if (product is null)
        {
            return NotFound();
        }

        updatedProduct.Id = product.Id;
        await _productsService.UpdateAsync(id, updatedProduct);

        return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete(string id)
    {
        var product = await _productsService.getAsync(id);
        if (product is null)
        {
            return NotFound();
        }

        await _productsService.RemoveAsync(id);

        return NoContent();
    }
}