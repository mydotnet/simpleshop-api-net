﻿using Microsoft.AspNetCore.Mvc;
using SimpleShop_API_NET.Models;
using SimpleShop_API_NET.Services;

namespace SimpleShop_API_NET.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrderProductsController : ControllerBase
{
    private readonly OrderProductsService _orderProductsService;

    public OrderProductsController(OrderProductsService orderProductsService)
    {
        _orderProductsService = orderProductsService;
    }

    [HttpGet("{orderId:length(24)}")]
    public async Task<List<OrderProduct>> Get(string orderId) =>
        await _orderProductsService.GetAsync(orderId);

    [HttpGet("{orderId:length(24)}&{productId:length(24)}")]
    public async Task<ActionResult<OrderProduct>> Get(string orderId, string productId)
    {
        var orderProduct = await _orderProductsService.GetAsync(orderId, productId);

        return (orderProduct is null) ? NotFound() : orderProduct;
    }
}