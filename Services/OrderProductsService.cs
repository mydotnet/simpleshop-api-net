﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SimpleShop_API_NET.Models;

namespace SimpleShop_API_NET.Services;

public class OrderProductsService
{
    private readonly IMongoCollection<OrderProduct> _orderProductCollection;

    public OrderProductsService(
        IOptions<SimpleShopDatabaseSettings> simpleShopDatabaseSettings)
    {
        var mongoClient = new MongoClient(simpleShopDatabaseSettings.Value.ConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(simpleShopDatabaseSettings.Value.DatabaseName);
        _orderProductCollection =
            mongoDatabase.GetCollection<OrderProduct>(simpleShopDatabaseSettings.Value.OrderProductCollectionName);
    }

    public async Task<List<OrderProduct>> GetAsync(string orderId) =>
        await _orderProductCollection.Find(x => x.OrderId == orderId).ToListAsync();

    public async Task<OrderProduct?> GetAsync(string orderId, string productId) =>
        await _orderProductCollection
            .Find(x => x.OrderId == orderId && x.ProductId == productId)
            .FirstOrDefaultAsync();

    public async Task CreateAsync(OrderProduct newOrderProduct) =>
        await _orderProductCollection.InsertOneAsync(newOrderProduct);

    public async Task UpdateAsync(string id, OrderProduct updatedOrderProduct) =>
        await _orderProductCollection.ReplaceOneAsync(x => x.Id == id, updatedOrderProduct);

    public async Task RemoveAsync(string id) =>
        await _orderProductCollection.DeleteOneAsync(x => x.Id == id);
}