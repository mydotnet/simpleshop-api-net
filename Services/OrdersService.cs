﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using SimpleShop_API_NET.DTO;
using SimpleShop_API_NET.Models;

namespace SimpleShop_API_NET.Services;

public class OrdersService
{
    private readonly IMongoCollection<Order> _orderCollection;
    private readonly IMongoCollection<Customer> _customerCollection;
    private readonly IMongoCollection<OrderProduct> _orderProductCollection;

    public OrdersService(
        IOptions<SimpleShopDatabaseSettings> simpleShopDatabaseSettings
    )
    {
        var mongoClient = new MongoClient(simpleShopDatabaseSettings.Value.ConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(simpleShopDatabaseSettings.Value.DatabaseName);
        _orderCollection =
            mongoDatabase.GetCollection<Order>(simpleShopDatabaseSettings.Value.OrderCollectionName);
        _customerCollection =
            mongoDatabase.GetCollection<Customer>(simpleShopDatabaseSettings.Value.CustomerCollectionName);
        _orderProductCollection =
            mongoDatabase.GetCollection<OrderProduct>(simpleShopDatabaseSettings.Value.OrderProductCollectionName);
    }

    public async Task<List<OrderResult>> GetAsync()
    {
        var ordersQuery = _orderCollection.AsQueryable();
        var orderProductsQuery = _orderProductCollection.AsQueryable();

        /*NOT WORKED!
        var query = ordersQuery.Where(o => true)
            .Select(o => new
            {
                o,
                orderProducts = orderProductsQuery.Where(od => od.OrderId == o.Id)
                    .Select(od => od)
            });*/

        /*NOT WORKED!
         var query = ordersQuery.Where(o => true)
            .Select(o => new OrderResult()
            {
                Id = o.Id,
                CustomerId = o.CustomerId,
                Note = o.Note,
                ReceiverAddress = o.ReceiverAddress,
                ReceiverName = o.ReceiverName,
                ReceiverPhoneNumber = o.ReceiverPhoneNumber,
                TotalAmount = o.TotalAmount,
                TotalQuantity = o.TotalQuantity,
                CreatedAt = o.CreatedAt,
                UpdatedAt = o.UpdatedAt,
                OrderProducts = orderProductsQuery.Where(od => od.OrderId == o.Id).ToList()
            });*/

        var result = from o in ordersQuery.Where(o => true)
            join od in orderProductsQuery on o.Id equals od.OrderId into ood
            select new OrderResult()
            {
                Id = o.Id,
                CustomerId = o.CustomerId,
                Note = o.Note,
                ReceiverAddress = o.ReceiverAddress,
                ReceiverName = o.ReceiverName,
                ReceiverPhoneNumber = o.ReceiverPhoneNumber,
                TotalAmount = o.TotalAmount,
                TotalQuantity = o.TotalQuantity,
                CreatedAt = o.CreatedAt,
                UpdatedAt = o.UpdatedAt,
                OrderProducts = ood.ToList()
            };

        return await Task.FromResult(result.ToList());
    }

    public async Task<OrderResult?> GetAsync(string id)
    {
        var ordersQuery = _orderCollection.AsQueryable();
        var orderProductsQuery = _orderProductCollection.AsQueryable();

        var result = from o in ordersQuery.Where(o => o.Id == id)
            join od in orderProductsQuery on o.Id equals od.OrderId into ood
            select new OrderResult()
            {
                Id = o.Id,
                CustomerId = o.CustomerId,
                Note = o.Note,
                ReceiverAddress = o.ReceiverAddress,
                ReceiverName = o.ReceiverName,
                ReceiverPhoneNumber = o.ReceiverPhoneNumber,
                TotalAmount = o.TotalAmount,
                TotalQuantity = o.TotalQuantity,
                CreatedAt = o.CreatedAt,
                UpdatedAt = o.UpdatedAt,
                OrderProducts = ood.ToList()
            };

        return await Task.FromResult(result.FirstOrDefault());
    }

    public async Task<List<Order>> GetSimpleAsync() =>
        await _orderCollection.Find(_ => true).ToListAsync();

    public async Task<Order?> GetSimpleAsync(string id) =>
        await _orderCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task<Order> CreateAsync(CreateOrder createOrder)
    {
        /* validate */
        var error = this.ValidateCreateOrder(createOrder);
        if (error != "")
        {
            throw new Exception(error);
        }

        /* find or create new customer */
        var customer = string.IsNullOrEmpty(createOrder.CustomerId)
            ? null
            : await _customerCollection.Find(x => x.Id == createOrder.CustomerId).FirstOrDefaultAsync();
        if (customer is null)
        {
            customer = new Customer()
            {
                Name = createOrder.ReceiverName,
                Address = createOrder.ReceiverAddress,
                PhoneNumber = createOrder.ReceiverPhoneNumber
            };
            await _customerCollection.InsertOneAsync(customer);
        }

        /* create new order */
        var newOrder = new Order
        {
            CustomerId = customer.Id,
            Note = createOrder.Note,
            ReceiverAddress = createOrder.ReceiverAddress,
            ReceiverName = createOrder.ReceiverName,
            ReceiverPhoneNumber = createOrder.ReceiverPhoneNumber
        };

        // total amount
        float totalAmount = 0;
        var totalQuantity = 0;
        createOrder.CreateOrderProducts.ForEach(x =>
        {
            totalAmount += x.ProductPrice * x.ProductQuantity;
            totalQuantity += x.ProductQuantity;
        });
        newOrder.TotalAmount = totalAmount;
        newOrder.TotalQuantity = totalQuantity;

        await _orderCollection.InsertOneAsync(newOrder);

        /* remove existing and re-create new orderProducts */
        // remove existing
        var productIds = createOrder.CreateOrderProducts.Select(od => od.ProductId).ToList();
        await _orderProductCollection.DeleteManyAsync(
            x => (x.OrderId == newOrder.Id && productIds.Contains(x.ProductId))
        );

        // re-create
        var ods = createOrder.CreateOrderProducts.Select(
            orderProduct => new OrderProduct()
            {
                OrderId = newOrder.Id,
                ProductId = orderProduct.ProductId,
                ProductName = orderProduct.ProductName,
                ProductDescription = orderProduct.ProductDescription,
                ProductPrice = orderProduct.ProductPrice,
                ProductQuantity = orderProduct.ProductQuantity
            }
        ).ToList();

        await _orderProductCollection.InsertManyAsync(ods);

        return newOrder;
    }

    public async Task UpdateAsync(string id, Order updatedOrder) =>
        await _orderCollection.ReplaceOneAsync(x => x.Id == id, updatedOrder);

    public async Task RemoveAsync(string id) =>
        await _orderCollection.DeleteOneAsync(x => x.Id == id);

    private string ValidateCreateOrder(CreateOrder createOrder)
    {
        if (createOrder.ReceiverAddress == "")
        {
            return "ReceiverAddress could not be an empty string";
        }

        if (createOrder.ReceiverName == "")
        {
            return "ReceiverName could not be an empty string";
        }

        if (createOrder.ReceiverPhoneNumber == "")
        {
            return "ReceiverPhoneNumber could not be an empty string";
        }

        if (createOrder.CreateOrderProducts is not List<CreateOrderProduct> orderProducts || orderProducts.Count < 1)
        {
            return "CreateOrderProducts could not be an empty string";
        }

        var createOrderProductsError = new List<string>();
        orderProducts.ForEach(x =>
        {
            if (string.IsNullOrEmpty(x.ProductId) ||
                x.ProductPrice < 1 ||
                x.ProductQuantity < 1
               )
            {
                createOrderProductsError.Add(
                    "Expected either OrderId or ProductId or ProductPrice or ProductQuantity");
            }
        });
        if (createOrderProductsError.Count > 0)
        {
            return "Invalid CreateOrderProducts.Detail: " + string.Join(", ", createOrderProductsError);
        }

        return "";
    }
}