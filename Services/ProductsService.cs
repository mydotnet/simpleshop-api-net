﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SimpleShop_API_NET.Models;

namespace SimpleShop_API_NET.Services;

public class ProductsService
{
    private readonly IMongoCollection<Product> _productCollection;

    public ProductsService(
        IOptions<SimpleShopDatabaseSettings> simpleShopDatabaseSettings)
    {
        var mongoClient = new MongoClient(simpleShopDatabaseSettings.Value.ConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(simpleShopDatabaseSettings.Value.DatabaseName);
        _productCollection =
            mongoDatabase.GetCollection<Product>(simpleShopDatabaseSettings.Value.ProductCollectionName);
    }

    public async Task<List<Product>> getAsync() => 
        await _productCollection.Find(_ => true).ToListAsync();

    public async Task<Product?> getAsync(string id) =>
        await _productCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task CreateAsync(Product newProduct) => 
        await _productCollection.InsertOneAsync(newProduct);

    public async Task UpdateAsync(string id, Product updatedProduct) =>
        await _productCollection.ReplaceOneAsync(x => x.Id == id, updatedProduct);

    public async Task RemoveAsync(string id) => 
        await _productCollection.DeleteOneAsync(x => x.Id == id);
}