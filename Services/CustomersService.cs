﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SimpleShop_API_NET.Models;

namespace SimpleShop_API_NET.Services;

public class CustomersService
{
    private readonly IMongoCollection<Customer> _customerCollection;

    public CustomersService(
        IOptions<SimpleShopDatabaseSettings> simpleShopDatabaseSettings)
    {
        var mongoClient = new MongoClient(simpleShopDatabaseSettings.Value.ConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(simpleShopDatabaseSettings.Value.DatabaseName);
        _customerCollection =
            mongoDatabase.GetCollection<Customer>(simpleShopDatabaseSettings.Value.CustomerCollectionName);
    }

    public async Task<List<Customer>> GetAsync() =>
        await _customerCollection.Find(_ => true).ToListAsync();

    public async Task<Customer?> GetAsync(string id) =>
        await _customerCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task CreateAsync(Customer newCustomer) =>
        await _customerCollection.InsertOneAsync(newCustomer);

    public async Task UpdateAsync(string id, Customer updatedCustomer) =>
        await _customerCollection.ReplaceOneAsync(x => x.Id == id, updatedCustomer);

    public async Task RemoveAsync(string id) =>
        await _customerCollection.DeleteOneAsync(x => x.Id == id);
}