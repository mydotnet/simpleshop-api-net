using SimpleShop_API_NET.Models;
using SimpleShop_API_NET.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.Configure<SimpleShopDatabaseSettings>(builder.Configuration.GetSection("SimpleShopDatabase"));

builder.Services.AddSingleton<CustomersService>();
builder.Services.AddSingleton<OrdersService>();
builder.Services.AddSingleton<OrderProductsService>();
builder.Services.AddSingleton<ProductsService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
