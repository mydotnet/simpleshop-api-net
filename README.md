﻿# SimpleShop-API-NET

Welcome to SimpleShop-API-NET

## Prerequisites

1. Net Core 6 SDK from [.NET 6.0](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
    - Included runtimes
    - .NET Runtime 6.0.15
    - ASP.NET Core Runtime 6.0.15
    - .NET Desktop Runtime 6.0.15

2. .Net develop package from [.NET Framework 4.8.1](https://dotnet.microsoft.com/en-us/download/visual-studio-sdks)
    - Select .NET Framework 4.8.1 > Developer Pack

3. IDE: Rider from [Download Rider Windows](https://www.jetbrains.com/rider/download/#section=windows)

## Run

1. Nuget restore package

2. Net build
   ```
   dotnet build .\SimpleShop-API-NET.csproj
   ```
3. Run
   ```
   dotnet run --launch-profile SimpleShop_API_NET
   ```
4. Browse to [localhost:7205 swagger](https://localhost:7205/swagger/index.html) and enjoy!

## Detail steps

1. Create new webapi project from template
   ```
   dotnet create webapi -o SimpleShop-API-NET
   cd SimpleShop-API-NET
   dotnet add package Microsoft.EntityFrameworkCore.InMemory
   ```
   
2. Run existing code
   ```
   dotnet dev-certs https --trust
   dotnet run --launch-profile SimpleShop_API_NET
   ```
   Browse to `https://localhost:7205/swagger/index.html` to get first result
   
3. Config MongoDB and add Models
   ```
   dotnet add package MongoDB.Driver
   ```
   Update appsettings.json and Program.cs for Database and Models configuration

4. Add Services for CRUD
   Inject _collection, _databaseSetting, CRUD
   Update Program.cs to add new services

5. Add Controller

## Todos

1. [x] Init
2. [x] CRUD Product, Order, Customer
3. [ ] CRUD User, authentication JWT
4. [ ] Grant permission

---

## References
- https://learn.microsoft.com/vi-vn/aspnet/core/tutorials/first-web-api?view=aspnetcore-6.0&tabs=visual-studio-code